<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    //

    protected $fillable = [
        'placa', 'brand', 'type_vehicle', 'owner_id'
    ];

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }
}
