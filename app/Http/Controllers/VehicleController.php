<?php

namespace App\Http\Controllers;

use App\Owner;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $vehicles = Vehicle::all();
        return response(['vehicles' => $vehicles], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'placa' => 'required|regex:/^[a-zA-Z]{3}[0-9]{2}[a-zA-Z0-9]{1}$/|unique:vehicles,placa',
            'brand' => 'required',
            'type_vehicle' => 'required',
        ], [
            'placa.required' => 'Ingrese la placa del vehiculo',
            'placa.regex' => 'Ingrese una placa valida',
            'placa.unique' => 'Este vehiculo se encuentra registrado',
            'brand.required' => 'Ingrese la marca del vehiculo',
            'type_vehicle.required' => 'seleccione el tipo de vehiculo',
        ]);
        if ($request->owner_id) {
            $this->validate($request, [
                'owner_id' => 'required|numeric',
            ], [
                'owner_id.required' => 'Error al seleccionar propietario',
                'owner_id.numeric' => 'Error al seleccionar propietario',
            ]);
        } else {
            $this->validate($request, [
                'name' => 'required',
                'identification' => 'required|numeric|unique:owners,identification',
            ], [
                'name.required' => 'Ingrese el nombre del propietario',
                'identification.required' => 'Ingrese el numero de identificacion',
                'identification.numeric' => 'La identificacion solo es en numero',
                'identification.unique' => 'Este propietario se encuentra registrado',
            ]);
            $owner = Owner::create([
                'name' => ucwords($request->name),
                "identification" => $request->identification
            ]);
        }
        Vehicle::create([
            'placa' => strtoupper($request->placa),
            'brand' => ucwords($request->brand),
            'type_vehicle' => strtoupper($request->type_vehicle),
            'owner_id' => $request->owner_id ? $request->owner_id : $owner->id,
        ]);

        return response(['success' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Vehicle $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Vehicle $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $vehicle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Vehicle $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


    }

    public function vehicleBrand()
    {
        $vehicles = DB::table('vehicles')
            ->select('brand', DB::raw('count(*) as total'))
            ->groupBy('brand')
            ->get();
        return response(['vehicles' => $vehicles], 200);
    }

    public function find(Request $request)
    {
        //
        $vehiclesResponse = array();
        if ($request->find == "PLACA") {
            $vehicle = Vehicle::where('placa', $request->text)->first();
            if ($vehicle) {
                $data = [
                    "placa" => $vehicle->placa,
                    "brand" => $vehicle->brand,
                    "type_vehicle" => $vehicle->type_vehicle,
                    "owner" => $vehicle->owner,
                ];
                array_push($vehiclesResponse, $data);
            }

        } else if ($request->find == "NAME") {
            $owners = Owner::where("name", 'like', $request->text . '%')->get();
            foreach ($owners as $owner) {
                foreach ($owner->vehicles as $vehicle) {
                    $data = [
                        "placa" => $vehicle->placa,
                        "brand" => $vehicle->brand,
                        "type_vehicle" => $vehicle->type_vehicle,
                        "owner" => $owner,
                    ];
                    array_push($vehiclesResponse, $data);
                }
            }
        } else if ($request->find == "IDENTIFICATION") {
            $owner = Owner::where("identification", $request->text)->first();
            if ($owner) {
                foreach ($owner->vehicles as $vehicle) {
                    $data = [
                        "placa" => $vehicle->placa,
                        "brand" => $vehicle->brand,
                        "type_vehicle" => $vehicle->type_vehicle,
                        "owner" => $owner,
                    ];
                    array_push($vehiclesResponse, $data);
                }
            }
            return response(['success' => true, 'vehicle' => $vehiclesResponse, 'owner' => $owner], 200);

        } else {
            return response(['success' => false, 'vehicle' => $vehiclesResponse], 200);
        }
        return response(['success' => true, 'vehicle' => $vehiclesResponse], 200);

    }

}
