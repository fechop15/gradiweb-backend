<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class VehicleTest extends TestCase
{
    use WithoutMiddleware;


    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testRegisterVehicleAndOwner()
    {
        $this->artisan('migrate:fresh');

        $data = [
            'placa' => 'Abc223',
            'brand' => 'mazda',
            'type_vehicle' => 'carro',
            'name' => 'fredy',
            'identification' => 123456789,
        ];
        $response = $this->json('POST', route('vehicle.store'), $data);
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);
    }

    public function testRegisterVehicleToOwner()
    {
        $data = [
            'placa' => 'Abb223',
            'brand' => 'audy',
            'type_vehicle' => 'carro',
            'owner_id' => 1,
        ];
        $response = $this->json('POST', route('vehicle.store'), $data);
        $response->assertStatus(201);
        $response->assertJson(['success' => true]);
    }

    public function testFindPlaca()
    {
        $data = [
            'text' => 'Abc223',
            'find' => 'PLACA',
        ];
        $response = $this->json('GET', route('vehicle.find'), $data);
        $response->assertStatus(200);
        $response->assertJson(['success' => true]);
    }

    public function testFindName()
    {
        $data = [
            'text' => 'fredy',
            'find' => 'NAME',
        ];
        $response = $this->json('GET', route('vehicle.find'), $data);
        $response->assertStatus(200);
        $response->assertJson(['success' => true]);
    }

    public function testFindIdentification()
    {
        $data = [
            'text' => '123456789',
            'find' => 'IDENTIFICATION',
        ];
        $response = $this->json('GET', route('vehicle.find'), $data);
        $response->assertStatus(200);
        $response->assertJson(['success' => true]);
    }
}
