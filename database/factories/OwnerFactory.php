<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Owner::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'identification' => $faker->unique()->randomDigit,
    ];
});
