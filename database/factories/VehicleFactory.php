<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\App\Vehicle::class, function (Faker $faker) {
    $brand = array("MAZDA", "BMW", "CHEBROLET", "OTRO");
    $type_vehicle = array("Bicicleta", "Ciclomotor", "Motocicleta", "Motocarro");
    return [
        //
        'placa' => Str::random(3) . rand(100, 999),
        'brand' => $brand[array_rand($brand)],
        'type_vehicle' => $type_vehicle[array_rand($type_vehicle)],
        'owner_id' => factory(App\Owner::class),
    ];
});
